# Cratis Server

## Requirements
 - Node.js v14
 - npm

## Building
To build server you need to install Node.js v14  
https://nodejs.org/dist/latest-v14.x/  
This package should also include npm

Then in `Server` directory simply run:
```
npm install
npm run build-client
npm start
```
---

To automatically rebuild client view (basically web page), after client code changes, you can also run
```
npm run watch-client
```

Remeber that after server code changes you need to restart the server.

## Testing
To test whether server works correctly you can visit locally http://localhost:8080/test
or externaly in you local area network `192.168.X.Y:8080/test`
