import ejs from 'ejs';
import fastify from 'fastify';
import fastifyStatic from 'fastify-static';
import path from 'path';
import pino from 'pino';
import pointOfView from 'point-of-view';

async function main() {
	const logger = pino({
		prettyPrint: true
	});
	const server = fastify({
		logger: logger
	});

	// Serve both assets & compiled frontend JS
	server.register(fastifyStatic, {
		root: [
			path.resolve(__dirname, '../../dist/client'),
			path.resolve(__dirname, '../../assets')
		],
		prefix: '/assets'
	});

	// Configure template rendering
	server.register(pointOfView, {
		engine: { ejs: ejs },
		templates: path.resolve(__dirname, './templates'),
		includeViewExtension: true
	});

	// Define individual view routes like this
	server.get('/test', async (req, res) => {
		return res.view('view', {
			view: 'test',
			title: 'Some Majestic Title'
		});
	});
	
	let clicks = 0;
	server.post('/inc_clicks', async (req, res) => {
		clicks+=1;
		console.log(clicks);
		return res.code(200).send({clicks: clicks});
	});
	
	

	await server.listen(8080, "0.0.0.0");
};

main();