'use strict';

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const sass = require('node-sass');

module.exports = {
	mode: process.env.NODE_ENV || 'development',
	devtool: 'source-map',
	entry: {
		main: [
			path.resolve(__dirname, './index.tsx'),
			path.resolve(__dirname, './style.scss')
		]
	},
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, '../../dist/client')
	},
	resolve: {
		extensions: [ '.js', '.ts', '.tsx' ]
	},
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				use: [{ loader: 'ts-loader' }]
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							implementation: sass,
							sassOptions: { fiber: false }
						}
					}
				]
			},
			{
				test: /\.(svg|ttf|eot|woff|woff2|jpg|jpeg|png|gif)$/i,
				type: 'asset/resource'
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin()
	]
};