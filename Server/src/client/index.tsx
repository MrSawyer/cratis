import * as React from 'react';
import * as ReactDOM from 'react-dom';
import TestView from './views/TestView';

// We know for a fact body is not gonna be null, that's what the "!" at the end
//  tells the transpiler.
const body = document.querySelector('body')!;

const views: Record<string, React.ReactNode> = {
	test: <TestView/>
};

const viewName = body.dataset.view;
if (!viewName) {
	throw new Error('<body> must have a "data-view" attribute');
}

const view = views[viewName];
if (!view) {
	throw new Error(`No such view defined: ${viewName}`);
}

const toRender = view;

ReactDOM.render(<React.Fragment>{toRender}</React.Fragment>, document.getElementById('reactMainContainer'));