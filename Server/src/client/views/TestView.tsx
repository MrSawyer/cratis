import * as React from 'react';
import { useState } from 'react';

export default function TestView() : JSX.Element {
	const [ counter, setCounter ] = useState<number>(0);
	const [ globalCounter, setGlobalCounter ] = useState<number>(0);


	const logo = <img src="/assets/logo.png"/>;

	const counterDisplay = <p>You have clicked the button {counter} times!</p>;
	const globalCounterDisplay = <p>Global button was clicked {globalCounter} times!</p>;

	const someButton = <button onClick={event => setCounter(counter + 1)}>
		Fuck ASP.NET
	</button>;

	const globalButton = <button onClick = {
		async event => 
		{
			const response = await fetch('/inc_clicks', {method: 'POST'});
			if(response.ok)
			{
				const resJson = await response.json();
				setGlobalCounter(resJson.clicks);
				console.log(resJson);
			}

			console.log(response);
		}
		
	}> Global Button </button>;

	return <div>
		{logo}
		{counterDisplay}
		{globalCounterDisplay}
		{someButton}
		{globalButton}
	</div>;
};