#include "texture-loader.h"

#include <FreeImage.h>

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::loaders
{
	Texture* loadTextureFromFile(std::string path, BOOL srgb)
	{
		HWND window_handle = system::getWindowHandle();

		FIBITMAP* image_raw = FreeImage_Load(FreeImage_GetFIFFromFilename(path.c_str()), path.c_str());
		if (image_raw == nullptr)
		{
			std::string error_message = "Unable to open file: \"" + path + "\".";
			MessageBox(window_handle, error_message.c_str(), "Texture loader error", MB_ICONERROR | MB_OK);
			return nullptr;
		}

		FIBITMAP* image_32bit = FreeImage_ConvertTo32Bits(image_raw);
		if (image_32bit == nullptr)
		{
			std::string error_message = "Conversion to 32 bits bitmap failed. File path: \"" + path + "\".";
			MessageBox(window_handle, error_message.c_str(), "Texture loader error", MB_ICONERROR | MB_OK);
			FreeImage_Unload(image_raw);
			return nullptr;
		}

		UINT image_width = FreeImage_GetWidth(image_32bit);
		UINT image_height = FreeImage_GetHeight(image_32bit);

		Texture* texture = createComponent<Texture>();
		texture->setTextureDataBGRA(FreeImage_GetBits(image_32bit), image_width, image_height, srgb);

		FreeImage_Unload(image_32bit);
		FreeImage_Unload(image_raw);

		return texture;
	}
}
