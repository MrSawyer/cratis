#pragma once

#include "../Components/texture.h"
#include <string>

namespace Renderer::loaders
{
	using namespace components;

	Texture*	loadTextureFromFile(std::string path, BOOL srgb = FALSE);
}
