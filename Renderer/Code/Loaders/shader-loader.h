#pragma once

#include "../Components/shader.h"
#include <string>

namespace Renderer::loaders
{
	using namespace components;

	Shader* loadShaderFromFile(std::string path);
	Shader* loadShaderFromFile(std::string vertex_shader_path, std::string pixel_shader_path);
}
