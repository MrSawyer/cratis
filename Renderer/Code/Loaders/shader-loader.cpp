#include "shader-loader.h"

#include <fstream>
#include <sstream>

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::loaders
{
	Shader* loadShaderFromFile(std::string path)
	{
		HWND window_handle = system::getWindowHandle();

		std::ifstream file(path);
		if (!file.good())
		{
			std::string error_message = "Unable to open file: \"" + path + "\".";
			MessageBox(window_handle, error_message.c_str(), "Shader loader error", MB_ICONERROR | MB_OK);
			file.close();
			return nullptr;
		}
		std::stringstream stream;
		stream << file.rdbuf();
		std::string shader_code = stream.str();
		file.close();

		Shader* shader = createComponent<Shader>();
		shader->setShaderCode(shader_code);

		return shader;
	}

	Shader* loadShaderFromFile(std::string vertex_shader_path, std::string pixel_shader_path)
	{
		HWND window_handle = system::getWindowHandle();

		std::ifstream file(vertex_shader_path);
		if (!file.good())
		{
			std::string error_message = "Unable to open file: \"" + vertex_shader_path + "\".";
			MessageBox(window_handle, error_message.c_str(), "Shader loader error", MB_ICONERROR | MB_OK);
			file.close();
			return nullptr;
		}
		std::stringstream stream;
		stream << file.rdbuf();
		std::string vertex_shader_code = stream.str();
		file.close();

		file.open(pixel_shader_path);
		if (!file.good())
		{
			std::string error_message = "Unable to open file: \"" + pixel_shader_path + "\".";
			MessageBox(window_handle, error_message.c_str(), "Shader loader error", MB_ICONERROR | MB_OK);
			file.close();
			return nullptr;
		}
		stream.clear();
		stream << file.rdbuf();
		std::string pixel_shader_code = stream.str();
		file.close();

		Shader* shader = createComponent<Shader>();
		shader->setVertexShaderCode(vertex_shader_code);
		shader->setPixelShaderCode(pixel_shader_code);

		return shader;
	}
}
