#pragma once

namespace Renderer::components
{
	namespace memory
	{
		class Component
		{
		protected:
			Component();
			Component(const Component&) = default;

		public:
			virtual void terminate() = 0;
			virtual ~Component() = default;
		};

		void storeComponent(Component* component);
		void removeComponent(Component** component);
		void clearComponents();
	}

	template<class T> T* createComponent()
	{
		T* component = new T();
		memory::storeComponent(component);
		return component;
	}

	template<class T> void destroyComponent(T*& component)
	{
		memory::removeComponent((memory::Component**)&component);
		delete component;
		component = nullptr;
	}
}
