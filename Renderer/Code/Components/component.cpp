#include "component.h"

#include <unordered_set>

namespace Renderer::components
{
	namespace memory
	{
		Component::Component() {}

		std::unordered_set<Component*> Components;

		void storeComponent(Component* component)
		{
			Components.emplace(component);
		}

		void removeComponent(Component** component)
		{
			(*component)->terminate();
			Components.erase(*component);
		}

		void clearComponents()
		{
			for (auto& component : Components)
			{
				component->terminate();
				delete component;
			}
			Components.clear();
		}
	}
}
