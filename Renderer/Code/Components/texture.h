#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <iostream>
#include <array>

#include "component.h"

namespace Renderer::components
{
	class Texture : public memory::Component
	{
	private:
		ID3D11Texture2D*				buffer;
		D3D11_TEXTURE2D_DESC			buffer_desc;
		D3D11_SUBRESOURCE_DATA			buffer_data;
		void*							buffer_bytes;
		ID3D11ShaderResourceView*		resource;
		D3D11_SHADER_RESOURCE_VIEW_DESC	resource_desc;

	public:
		Texture();
		Texture(const Texture&) = delete;
		~Texture() = default;

	public:
		void terminate() override;

	public:
		bool makeTexture();
		void freeTexture();
		void bindTextureToVertexShader(UINT slot = 0);
		void bindTextureToPixelShader(UINT slot = 0);

	public:
		void setTextureData(FLOAT* image, UINT width, UINT height, UINT dimensions);
		void setTextureData(UINT* image, UINT width, UINT height, UINT dimensions);
		void setTextureData(BYTE* image, UINT width, UINT height, UINT dimensions = 4, BOOL srgb = FALSE);
		void setTextureDataBGRA(BYTE* image, UINT width, UINT height, BOOL srgb = FALSE);
	};
}
