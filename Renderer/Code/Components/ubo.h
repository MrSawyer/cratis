#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <iostream>

#include "component.h"

namespace Renderer::components
{
	struct UniformData {};

	class UBO : public memory::Component
	{
	private:
		ID3D11Buffer*			buffer;
		D3D11_BUFFER_DESC		buffer_desc;
		D3D11_SUBRESOURCE_DATA	buffer_data;

	public:
		UBO();
		UBO(const UBO&) = delete;
		~UBO() = default;

	public:
		void terminate() override;

	public:
		bool makeBuffer();
		void freeBuffer();
		void bindBufferToVertexShader(UINT slot = 0);
		void bindBufferToPixelShader(UINT slot = 0);

	public:
		void setBufferData(UniformData& data, size_t size);
		bool updateBufferData(UniformData& data, size_t size);
	};
}
