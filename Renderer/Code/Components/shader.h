#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <iostream>
#include <string>
#include <vector>

#include "component.h"

namespace Renderer::components
{
	class Shader : public memory::Component
	{
	private:
		ID3D11VertexShader* vertex_shader;
		ID3D10Blob*			vs_buffer;
		std::string			vs_code;

		ID3D11PixelShader*	pixel_shader;
		ID3D10Blob*			ps_buffer;
		std::string			ps_code;

		ID3D11InputLayout*						input_layout;
		std::vector<D3D11_INPUT_ELEMENT_DESC>	input_elements;

	public:
		Shader();
		Shader(const Shader&) = delete;
		~Shader() = default;

	public:
		void terminate() override;

	public:
		bool makeShader();
		void freeShader();
		void bindShader();

	public:
		void appendInputLayoutElement(std::string name, DXGI_FORMAT format, UINT index = 0);
		void setVertexShaderCode(std::string vertex_shader_code);
		void setPixelShaderCode(std::string pixel_shader_code);
		void setShaderCode(std::string shader_code);

	public:
		std::string getVertexShaderCode() const;
		std::string getPixelShaderCode() const;
	};
}
