#include "ubo.h"

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::components
{
	UBO::UBO()
	{
		buffer = nullptr;
		buffer_desc = { 0 };
		buffer_data = { 0 };

		buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		buffer_desc.MiscFlags = 0;
	}

	void UBO::terminate()
	{
		freeBuffer();
		std::cout << "UBO terminated." << std::endl;
	}

	bool UBO::makeBuffer()
	{
		if (buffer != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous UBO first.", "Warning", MB_ICONWARNING | MB_OK);
			return false;
		}

		if (FAILED(system::getDevice()->CreateBuffer(&buffer_desc, &buffer_data, &buffer)))
		{
			MessageBox(system::getWindowHandle(), "Creating UBO failed.", "Error", MB_ICONERROR | MB_OK);
			freeBuffer();
			return false;
		}

		return true;
	}

	void UBO::freeBuffer()
	{
		system::safeRelease(&buffer);
	}

	void UBO::bindBufferToVertexShader(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->VSSetConstantBuffers(slot, 1, &buffer);
	}

	void UBO::bindBufferToPixelShader(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->PSSetConstantBuffers(slot, 1, &buffer);
	}

	void UBO::setBufferData(UniformData& data, size_t size)
	{
		if (buffer != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous UBO first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		buffer_desc.ByteWidth = static_cast<UINT>(size);
		buffer_data.pSysMem = &data;
	}

	bool UBO::updateBufferData(UniformData& data, size_t size)
	{
		ID3D11DeviceContext* context = system::getContext();

		D3D11_MAPPED_SUBRESOURCE buffer_map = { 0 };
		if (SUCCEEDED(context->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &buffer_map)))
		{
			memcpy(buffer_map.pData, &data, size);
			context->Unmap(buffer, 0);
			return true;
		}

		return false;
	}
}
