#include "sampler.h"

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::components
{
	Sampler::Sampler()
	{
		sampler = nullptr;
		
		sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampler_desc.MipLODBias = 0.0F;
		sampler_desc.MaxAnisotropy = 0;
		sampler_desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampler_desc.BorderColor[0] = 0.0F;
		sampler_desc.BorderColor[1] = 0.0F;
		sampler_desc.BorderColor[2] = 0.0F;
		sampler_desc.BorderColor[3] = 0.0F;
		sampler_desc.MinLOD = 0.0F;
		sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
	}

	void Sampler::terminate()
	{
		freeSampler();
		std::cout << "Sampler terminated." << std::endl;
	}

	bool Sampler::makeSampler()
	{
		if (sampler != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous sampler first.", "Warning", MB_ICONWARNING | MB_OK);
			return false;
		}

		if (FAILED(system::getDevice()->CreateSamplerState(&sampler_desc, &sampler)))
		{
			MessageBox(system::getWindowHandle(), "Creating sampler state failed.", "Error", MB_ICONERROR | MB_OK);
			freeSampler();
			return false;
		}

		return true;
	}

	void Sampler::freeSampler()
	{
		system::safeRelease(&sampler);
	}

	void Sampler::bindSamplerToVertexShader(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->VSSetSamplers(slot, 1, &sampler);
	}

	void Sampler::bindSamplerToPixelShader(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->PSSetSamplers(slot, 1, &sampler);
	}

	void Sampler::setTextureFilter(D3D11_FILTER filter)
	{
		if (sampler != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous sampler first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		sampler_desc.Filter = filter;
	}

	void Sampler::setTextureWrapModeU(D3D11_TEXTURE_ADDRESS_MODE wrap_mode)
	{
		if (sampler != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous sampler first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		sampler_desc.AddressU = wrap_mode;
	}

	void Sampler::setTextureWrapModeV(D3D11_TEXTURE_ADDRESS_MODE wrap_mode)
	{
		if (sampler != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous sampler first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		sampler_desc.AddressV = wrap_mode;
	}

	D3D11_FILTER Sampler::getTextureFilter() const
	{
		return sampler_desc.Filter;
	}

	D3D11_TEXTURE_ADDRESS_MODE Sampler::getTextureWrapModeU() const
	{
		return sampler_desc.AddressU;
	}

	D3D11_TEXTURE_ADDRESS_MODE Sampler::getTextureWrapModeV() const
	{
		return sampler_desc.AddressV;
	}
}
