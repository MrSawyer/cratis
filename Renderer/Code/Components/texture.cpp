#include "texture.h"

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::components
{
	Texture::Texture()
	{
		buffer = nullptr;
		buffer_data = { 0 };
		buffer_bytes = nullptr;

		buffer_desc.MipLevels = 0;
		buffer_desc.ArraySize = 1;
		buffer_desc.SampleDesc.Count = 1;
		buffer_desc.SampleDesc.Quality = 0;
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

		resource = nullptr;

		resource_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		resource_desc.Texture2D.MostDetailedMip = 0;
		resource_desc.Texture2D.MipLevels = -1;
	}

	void Texture::terminate()
	{
		freeTexture();
		std::cout << "Texture terminated." << std::endl;
	}

	bool Texture::makeTexture()
	{
		HWND window_handle = system::getWindowHandle();

		if (buffer != nullptr || resource != nullptr)
		{
			MessageBox(window_handle, "Destroy previous texture first.", "Warning", MB_ICONWARNING | MB_OK);
			return false;
		}

		ID3D11Device*			device = system::getDevice();
		ID3D11DeviceContext*	context = system::getContext();

		if (FAILED(device->CreateTexture2D(&buffer_desc, nullptr, &buffer)))
		{
			MessageBox(window_handle, "Creating texture failed.", "Error", MB_ICONERROR | MB_OK);
			freeTexture();
			return false;
		}

		if (FAILED(device->CreateShaderResourceView(buffer, &resource_desc, &resource)))
		{
			MessageBox(window_handle, "Creating texture shader resource failed.", "Error", MB_ICONERROR | MB_OK);
			freeTexture();
			return false;
		}

		context->UpdateSubresource(buffer, 0, nullptr, buffer_data.pSysMem, buffer_data.SysMemPitch, buffer_data.SysMemSlicePitch);
		context->GenerateMips(resource);

		return true;
	}

	void Texture::freeTexture()
	{
		system::safeRelease(&resource);
		system::safeRelease(&buffer);
		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
	}

	void Texture::bindTextureToVertexShader(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->VSSetShaderResources(slot, 1, &resource);
	}

	void Texture::bindTextureToPixelShader(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->PSSetShaderResources(slot, 1, &resource);
	}

	void Texture::setTextureData(FLOAT* image, UINT width, UINT height, UINT dimensions)
	{
		if (buffer != nullptr || resource != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous texture first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN;
		UINT		pitch = 0;
		UINT		size = 0;
		switch (dimensions)
		{
		case 1:
			format = DXGI_FORMAT_R32_FLOAT;
			pitch = 1 * sizeof(FLOAT) * width;
			size = pitch * height;
			break;

		case 2:
			format = DXGI_FORMAT_R32G32_FLOAT;
			pitch = 2 * sizeof(FLOAT) * width;
			size = pitch * height;
			break;

		case 3:
			format = DXGI_FORMAT_R32G32B32_FLOAT;
			pitch = 3 * sizeof(FLOAT) * width;
			size = pitch * height;
			break;

		case 4:
			format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			pitch = 4 * sizeof(FLOAT) * width;
			size = pitch * height;
			break;

		default:
			return;
		}
		
		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
		buffer_bytes = malloc((size_t)size);
		if (buffer_bytes == nullptr)
		{
			MessageBox(system::getWindowHandle(), "Texture buffer data allocation failed.", "Error", MB_ICONERROR | MB_OK);
			return;
		}
		memcpy(buffer_bytes, image, size);

		buffer_desc.Width = width;
		buffer_desc.Height = height;
		buffer_desc.Format = format;
		buffer_data.pSysMem = buffer_bytes;
		buffer_data.SysMemPitch = pitch;
		buffer_data.SysMemSlicePitch = size;
	}

	void Texture::setTextureData(UINT* image, UINT width, UINT height, UINT dimensions)
	{
		if (buffer != nullptr || resource != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous texture first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN;
		UINT		pitch = 0;
		UINT		size = 0;
		switch (dimensions)
		{
		case 1:
			format = DXGI_FORMAT_R32_UINT;
			pitch = 1 * sizeof(UINT) * width;
			size = pitch * height;
			break;

		case 2:
			format = DXGI_FORMAT_R32G32_UINT;
			pitch = 2 * sizeof(UINT) * width;
			size = pitch * height;
			break;

		case 3:
			format = DXGI_FORMAT_R32G32B32_UINT;
			pitch = 3 * sizeof(UINT) * width;
			size = pitch * height;
			break;

		case 4:
			format = DXGI_FORMAT_R32G32B32A32_UINT;
			pitch = 4 * sizeof(UINT) * width;
			size = pitch * height;
			break;

		default:
			return;
		}

		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
		buffer_bytes = malloc((size_t)size);
		if (buffer_bytes == nullptr)
		{
			MessageBox(system::getWindowHandle(), "Texture buffer data allocation failed.", "Error", MB_ICONERROR | MB_OK);
			return;
		}
		memcpy(buffer_bytes, image, size);

		buffer_desc.Width = width;
		buffer_desc.Height = height;
		buffer_desc.Format = format;
		buffer_data.pSysMem = buffer_bytes;
		buffer_data.SysMemPitch = pitch;
		buffer_data.SysMemSlicePitch = size;
	}

	void Texture::setTextureData(BYTE* image, UINT width, UINT height, UINT dimensions, BOOL srgb)
	{
		if (buffer != nullptr || resource != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous texture first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN;
		UINT		pitch = 0;
		UINT		size = 0;
		switch (dimensions)
		{
		case 1:
			format = DXGI_FORMAT_R8_UNORM;
			pitch = 1 * sizeof(BYTE) * width;
			size = pitch * height;
			break;

		case 2:
			format = DXGI_FORMAT_R8G8_UNORM;
			pitch = 2 * sizeof(BYTE) * width;
			size = pitch * height;
			break;

		case 3:
			format = srgb ? DXGI_FORMAT_R8G8B8A8_UNORM_SRGB : DXGI_FORMAT_R8G8B8A8_UNORM;
			pitch = 4 * sizeof(BYTE) * width;
			size = pitch * height;
			break;

		case 4:
			format = srgb ? DXGI_FORMAT_R8G8B8A8_UNORM_SRGB : DXGI_FORMAT_R8G8B8A8_UNORM;
			pitch = 4 * sizeof(BYTE) * width;
			size = pitch * height;
			break;

		default:
			return;
		}

		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
		buffer_bytes = malloc((size_t)size);
		if (buffer_bytes == nullptr)
		{
			MessageBox(system::getWindowHandle(), "Texture buffer data allocation failed.", "Error", MB_ICONERROR | MB_OK);
			return;
		}
		memcpy(buffer_bytes, image, size);

		buffer_desc.Width = width;
		buffer_desc.Height = height;
		buffer_desc.Format = format;
		buffer_data.pSysMem = buffer_bytes;
		buffer_data.SysMemPitch = pitch;
		buffer_data.SysMemSlicePitch = size;
	}

	void Texture::setTextureDataBGRA(BYTE* image, UINT width, UINT height, BOOL srgb)
	{
		if (buffer != nullptr || resource != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous texture first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		UINT pitch = 4 * sizeof(BYTE) * width;
		UINT size = pitch * height;

		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
		buffer_bytes = malloc((size_t)size);
		if (buffer_bytes == nullptr)
		{
			MessageBox(system::getWindowHandle(), "Texture buffer data allocation failed.", "Error", MB_ICONERROR | MB_OK);
			return;
		}
		memcpy(buffer_bytes, image, size);

		buffer_desc.Width = width;
		buffer_desc.Height = height;
		buffer_desc.Format = srgb ? DXGI_FORMAT_B8G8R8A8_UNORM_SRGB : DXGI_FORMAT_B8G8R8A8_UNORM;
		buffer_data.pSysMem = buffer_bytes;
		buffer_data.SysMemPitch = pitch;
		buffer_data.SysMemSlicePitch = size;
	}
}
