#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <iostream>

#include "component.h"

namespace Renderer::components
{
	class Sampler : public memory::Component
	{
	private:
		ID3D11SamplerState*	sampler;
		D3D11_SAMPLER_DESC	sampler_desc;

	public:
		Sampler();
		Sampler(const Sampler&) = delete;
		~Sampler() = default;

	public:
		void terminate() override;

	public:
		bool makeSampler();
		void freeSampler();
		void bindSamplerToVertexShader(UINT slot = 0);
		void bindSamplerToPixelShader(UINT slot = 0);

	public:
		void setTextureFilter(D3D11_FILTER filter);
		void setTextureWrapModeU(D3D11_TEXTURE_ADDRESS_MODE wrap_mode);
		void setTextureWrapModeV(D3D11_TEXTURE_ADDRESS_MODE wrap_mode);

	public:
		D3D11_FILTER getTextureFilter() const;
		D3D11_TEXTURE_ADDRESS_MODE getTextureWrapModeU() const;
		D3D11_TEXTURE_ADDRESS_MODE getTextureWrapModeV() const;
	};
}
