#include "shader.h"

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::components
{
	Shader::Shader()
	{
		vertex_shader = nullptr;
		vs_buffer = nullptr;
		vs_code = "";

		pixel_shader = nullptr;
		ps_buffer = nullptr;
		ps_code = "";

		input_layout = nullptr;
	}

	void Shader::terminate()
	{
		freeShader();
		std::cout << "Shader terminated." << std::endl;
	}

	bool Shader::makeShader()
	{
		HWND window_handle = system::getWindowHandle();

		if (vs_buffer != nullptr || ps_buffer != nullptr || vertex_shader != nullptr || pixel_shader != nullptr || input_layout != nullptr)
		{
			MessageBox(window_handle, "Destroy previous Shader first.", "Warning", MB_ICONWARNING | MB_OK);
			return false;
		}

		ID3D11Device*	device = system::getDevice();
		ID3D10Blob*		error_buffer = nullptr;
		UINT			compilation_flags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
		compilation_flags |= D3DCOMPILE_DEBUG;
#endif

		if (FAILED(D3DCompile((LPCVOID)vs_code.c_str(), (SIZE_T)vs_code.length(), nullptr, nullptr, nullptr, "VSMain", "vs_5_0", compilation_flags, 0, &vs_buffer, &error_buffer)))
		{
			const std::string error_message = "Vertex shader compilation failed with an error: " + std::string((char*)error_buffer->GetBufferPointer());
			MessageBox(window_handle, error_message.c_str(), "Shader compilation error", MB_ICONERROR | MB_OK);
			error_buffer->Release();
			freeShader();
			return false;
		}

		if (FAILED(D3DCompile((LPCVOID)ps_code.c_str(), (SIZE_T)ps_code.length(), nullptr, nullptr, nullptr, "PSMain", "ps_5_0", compilation_flags, 0, &ps_buffer, &error_buffer)))
		{
			const std::string error_message = "Pixel shader compilation failed with an error: " + std::string((char*)error_buffer->GetBufferPointer());
			MessageBox(window_handle, error_message.c_str(), "Shader compilation error", MB_ICONERROR | MB_OK);
			error_buffer->Release();
			freeShader();
			return false;
		}

		if (FAILED(device->CreateVertexShader(vs_buffer->GetBufferPointer(), vs_buffer->GetBufferSize(), nullptr, &vertex_shader)))
		{
			MessageBox(window_handle, "Creating vertex shader failed.", "Error", MB_ICONERROR | MB_OK);
			freeShader();
			return false;
		}

		if (FAILED(device->CreatePixelShader(ps_buffer->GetBufferPointer(), ps_buffer->GetBufferSize(), nullptr, &pixel_shader)))
		{
			MessageBox(window_handle, "Creating pixel shader failed.", "Error", MB_ICONERROR | MB_OK);
			freeShader();
			return false;
		}

		if (FAILED(device->CreateInputLayout(input_elements.data(), (UINT)input_elements.size(), vs_buffer->GetBufferPointer(), vs_buffer->GetBufferSize(), &input_layout)))
		{
			MessageBox(window_handle, "Creating shader input layout failed.", "Error", MB_ICONERROR | MB_OK);
			freeShader();
			return false;
		}

		return true;
	}

	void Shader::freeShader()
	{
		for (auto& input_element : input_elements)
		{
			free((char*)input_element.SemanticName);
		}
		input_elements.clear();
		input_elements.resize(0);

		system::safeRelease(&input_layout);
		system::safeRelease(&pixel_shader);
		system::safeRelease(&vertex_shader);
		system::safeRelease(&ps_buffer);
		system::safeRelease(&vs_buffer);
	}

	void Shader::bindShader()
	{
		ID3D11DeviceContext* context = system::getContext();
		context->VSSetShader(vertex_shader, nullptr, 0);
		context->PSSetShader(pixel_shader, nullptr, 0);
		context->IASetInputLayout(input_layout);
	}

	void Shader::appendInputLayoutElement(std::string name, DXGI_FORMAT format, UINT index)
	{
		if (vs_buffer != nullptr || ps_buffer != nullptr || vertex_shader != nullptr || pixel_shader != nullptr || input_layout != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous Shader first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		D3D11_INPUT_ELEMENT_DESC input_element;
		input_element.SemanticName = _strdup(name.c_str());
		input_element.SemanticIndex = index;
		input_element.Format = format;
		input_element.InputSlot = 0;
		input_element.AlignedByteOffset = input_elements.empty() ? 0 : D3D11_APPEND_ALIGNED_ELEMENT;
		input_element.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		input_element.InstanceDataStepRate = 0;

		input_elements.push_back(input_element);
	}

	void Shader::setVertexShaderCode(std::string vertex_shader_code)
	{
		if (vs_buffer != nullptr || ps_buffer != nullptr || vertex_shader != nullptr || pixel_shader != nullptr || input_layout != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous Shader first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		vs_code = vertex_shader_code;
	}

	void Shader::setPixelShaderCode(std::string pixel_shader_code)
	{
		if (vs_buffer != nullptr || ps_buffer != nullptr || vertex_shader != nullptr || pixel_shader != nullptr || input_layout != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous Shader first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		ps_code = pixel_shader_code;
	}

	void Shader::setShaderCode(std::string shader_code)
	{
		if (vs_buffer != nullptr || ps_buffer != nullptr || vertex_shader != nullptr || pixel_shader != nullptr || input_layout != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous Shader first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		vs_code = shader_code;
		ps_code = shader_code;
	}

	std::string Shader::getVertexShaderCode() const
	{
		return vs_code;
	}

	std::string Shader::getPixelShaderCode() const
	{
		return ps_code;
	}
}
