#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <iostream>
#include <vector>

#include "component.h"

namespace Renderer::components
{
	class VBO : public memory::Component
	{
	private:
		ID3D11Buffer*				buffer;
		D3D11_BUFFER_DESC			buffer_desc;
		D3D11_SUBRESOURCE_DATA		buffer_data;
		void*						buffer_bytes;
		D3D11_PRIMITIVE_TOPOLOGY	buffer_topology;
		UINT						buffer_stride;
		UINT						buffer_offset;
		UINT						buffer_count;

	public:
		VBO();
		VBO(const VBO&) = delete;
		~VBO() = default;

	public:
		void terminate() override;

	public:
		bool makeBuffer();
		void freeBuffer();
		void bindBuffer(UINT slot = 0);
		void drawBuffer(UINT slot = 0);

	public:
		void setBufferTopology(D3D11_PRIMITIVE_TOPOLOGY topology);
		void setBufferData(std::vector<FLOAT>& vertices, UINT stride, UINT offset = 0);
		void setBufferData(FLOAT* vertices, UINT array_size, UINT stride, UINT offset = 0);

	public:
		D3D11_PRIMITIVE_TOPOLOGY getBufferTopology() const;
		UINT getVertexNumber() const;
	};
}
