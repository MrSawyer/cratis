#include "vbo.h"

#include "../System/window.h"
#include "../System/graphics.h"

namespace Renderer::components
{
	VBO::VBO()
	{
		buffer = nullptr;
		buffer_desc = { 0 };
		buffer_data = { 0 };
		buffer_bytes = nullptr;

		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;

		buffer_topology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		buffer_stride = 0;
		buffer_offset = 0;
		buffer_count = 0;
	}

	void VBO::terminate()
	{
		freeBuffer();
		std::cout << "VBO terminated." << std::endl;
	}

	bool VBO::makeBuffer()
	{
		if (buffer != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous VBO first.", "Warning", MB_ICONWARNING | MB_OK);
			return false;
		}

		if (FAILED(system::getDevice()->CreateBuffer(&buffer_desc, &buffer_data, &buffer)))
		{
			MessageBox(system::getWindowHandle(), "Creating VBO failed.", "Error", MB_ICONERROR | MB_OK);
			freeBuffer();
			return false;
		}

		return true;
	}

	void VBO::freeBuffer()
	{
		system::safeRelease(&buffer);
		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
	}

	void VBO::bindBuffer(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->IASetVertexBuffers(slot, 1, &buffer, &buffer_stride, &buffer_offset);
		context->IASetPrimitiveTopology(buffer_topology);
	}

	void VBO::drawBuffer(UINT slot)
	{
		ID3D11DeviceContext* context = system::getContext();
		context->IASetVertexBuffers(slot, 1, &buffer, &buffer_stride, &buffer_offset);
		context->IASetPrimitiveTopology(buffer_topology);
		context->Draw(buffer_count, 0);
	}

	void VBO::setBufferTopology(D3D11_PRIMITIVE_TOPOLOGY topology)
	{
		buffer_topology = topology;
	}

	void VBO::setBufferData(std::vector<FLOAT>& vertices, UINT stride, UINT offset)
	{
		if (buffer != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous VBO first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		UINT size = static_cast<UINT>(sizeof(FLOAT) * vertices.size());

		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
		buffer_bytes = malloc((size_t)size);
		if (buffer_bytes == nullptr)
		{
			MessageBox(system::getWindowHandle(), "Vertex buffer data allocation failed.", "Error", MB_ICONERROR | MB_OK);
			return;
		}
		memcpy(buffer_bytes, vertices.data(), size);

		buffer_desc.ByteWidth = size;
		buffer_data.pSysMem = buffer_bytes;
		buffer_stride = stride * sizeof(FLOAT);
		buffer_offset = offset * sizeof(FLOAT);
		buffer_count = static_cast<UINT>(vertices.size() / stride);
	}

	void VBO::setBufferData(FLOAT* vertices, UINT array_size, UINT stride, UINT offset)
	{
		if (buffer != nullptr)
		{
			MessageBox(system::getWindowHandle(), "Destroy previous VBO first.", "Warning", MB_ICONWARNING | MB_OK);
			return;
		}

		UINT size = static_cast<UINT>(sizeof(FLOAT) * array_size);

		if (buffer_bytes != nullptr)
		{
			free(buffer_bytes);
			buffer_bytes = nullptr;
		}
		buffer_bytes = malloc((size_t)size);
		if (buffer_bytes == nullptr)
		{
			MessageBox(system::getWindowHandle(), "Vertex buffer data allocation failed.", "Error", MB_ICONERROR | MB_OK);
			return;
		}
		memcpy(buffer_bytes, vertices, size);

		buffer_desc.ByteWidth = size;
		buffer_data.pSysMem = buffer_bytes;
		buffer_stride = stride * sizeof(FLOAT);
		buffer_offset = offset * sizeof(FLOAT);
		buffer_count = static_cast<UINT>(array_size / stride);
	}

	D3D11_PRIMITIVE_TOPOLOGY VBO::getBufferTopology() const
	{
		return buffer_topology;
	}

	UINT VBO::getVertexNumber() const
	{
		return buffer_count;
	}
}
