#pragma once

#include <Windows.h>
#include <iostream>

#include "graphics.h"

namespace Renderer::system
{
	bool	initializeWindow(HINSTANCE instance);
	void	terminateWindow();
	void	activateWindow();

	const HWND& getWindowHandle();
}
