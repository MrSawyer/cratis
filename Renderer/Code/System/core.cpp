#include "core.h"

#include "../Components/component.h"
#include "../Presets/rectangle-preset.h"
#include "../Presets/triangle-preset.h"
#include "../Presets/circle-preset.h"

namespace Renderer
{
	bool initialize(HINSTANCE instance)
	{
		if (!system::initializeWindow(instance)) return false;
		if (!system::initializeGraphics(system::getWindowHandle())) return false;

		if (!presets::storage::initializeRectangle()) return false;
		if (!presets::storage::initializeTriangle()) return false;
		if (!presets::storage::initializeCircle()) return false;

		system::activateWindow();

		return true;
	}
	
	void terminate()
	{
		components::memory::clearComponents();
		system::terminateGraphics();
		system::terminateWindow();
	}

	int run()
	{
		MSG msgcontainer = { 0 };
		while (msgcontainer.message != WM_QUIT)
		{
			if (PeekMessage(&msgcontainer, 0, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msgcontainer);
				DispatchMessage(&msgcontainer);
			}
			else
			{
				system::renderGraphics();
			}
		}

		terminate();

#ifdef _DEBUG
		::system("pause");
#endif
		return static_cast<int>(msgcontainer.wParam);
	}
}
