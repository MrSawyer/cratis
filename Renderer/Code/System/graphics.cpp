#include "graphics.h"

#include "../testcode.h"

namespace Renderer::system
{
	ID3D11Device*			Device = nullptr;
	ID3D11DeviceContext*	Context = nullptr;
	ID3D11RenderTargetView* Target = nullptr;
	IDXGISwapChain*			SwapChain = nullptr;

	bool initializeGraphics(HWND window)
	{
		RECT windowclient;
		GetClientRect(window, &windowclient);

		UINT width = (UINT)(windowclient.right - windowclient.left);
		UINT height = (UINT)(windowclient.bottom - windowclient.top);

		DXGI_MODE_DESC mode_desc = { 0 };
		mode_desc.Width = width;
		mode_desc.Height = height;
		mode_desc.RefreshRate.Numerator = 0;
		mode_desc.RefreshRate.Denominator = 0;
		mode_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		mode_desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		mode_desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		DXGI_SWAP_CHAIN_DESC swapchain_desc = { 0 };
		swapchain_desc.BufferDesc = mode_desc;
		swapchain_desc.SampleDesc.Count = 8;
		swapchain_desc.SampleDesc.Quality = 0;
		swapchain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapchain_desc.BufferCount = 1;
		swapchain_desc.OutputWindow = window;
		swapchain_desc.Windowed = TRUE;
		swapchain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		swapchain_desc.Flags = 0;

		D3D_FEATURE_LEVEL features = D3D_FEATURE_LEVEL_11_0;
		if (FAILED(D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, 0, D3D11_CREATE_DEVICE_SINGLETHREADED, nullptr, 0, D3D11_SDK_VERSION, &swapchain_desc, &SwapChain, &Device, &features, &Context)))
		{
			MessageBox(0, "Initializing Direct3D 11 failed.", "Error", MB_ICONERROR | MB_OK);
			terminateGraphics();
			return false;
		}

		if (features != D3D_FEATURE_LEVEL_11_0)
		{
			MessageBox(0, "Direct3D 11 is not supported by your device.", "Error", MB_ICONERROR | MB_OK);
			terminateGraphics();
			return false;
		}

		return Testcode::onInit();
	}

	void terminateGraphics()
	{
		safeRelease(&SwapChain);
		safeRelease(&Target);
		safeRelease(&Context);
		safeRelease(&Device);
	}

	void renderGraphics()
	{
		Testcode::onUpdate();
		const FLOAT background[4] = { 0.16F, 0.16F, 0.16F, 1.0F };
		Context->ClearRenderTargetView(Target, background);
		Testcode::onDraw();
		SwapChain->Present(1, 0);
	}

	bool resizeGraphics(FLOAT width, FLOAT height)
	{
		if (Target != nullptr)
		{
			Context->OMSetRenderTargets(0, nullptr, nullptr);
			safeRelease(&Target);

			SwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
		}

		ID3D11Texture2D* backbuffer = nullptr;
		if (FAILED(SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbuffer)))
		{
			MessageBox(0, "Loading backbuffer object from a swapchain failed.", "Error", MB_ICONERROR | MB_OK);
			return false;
		}

		if (FAILED(Device->CreateRenderTargetView(backbuffer, nullptr, &Target)))
		{
			MessageBox(0, "Creating render target view failed.", "Error", MB_ICONERROR | MB_OK);
			safeRelease(&backbuffer);
			return false;
		}
		safeRelease(&backbuffer);

		D3D11_VIEWPORT viewport = { 0 };
		viewport.TopLeftX = 0.0F;
		viewport.TopLeftY = 0.0F;
		viewport.Width = width;
		viewport.Height = height;
		viewport.MinDepth = 0.0F;
		viewport.MaxDepth = 1.0F;

		Context->OMSetRenderTargets(1, &Target, nullptr);
		Context->RSSetViewports(1, &viewport);

		return true;
	}

	ID3D11Device*& getDevice()
	{
		return Device;
	}

	ID3D11DeviceContext*& getContext()
	{
		return Context;
	}

	ID3D11RenderTargetView*& getTarget()
	{
		return Target;
	}

	IDXGISwapChain*& getSwapChain()
	{
		return SwapChain;
	}
}
