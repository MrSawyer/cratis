#pragma once

#include <Windows.h>
#include <d3d11.h>
#include <iostream>
#include <math.h>

namespace Renderer::system
{
	bool initializeGraphics(HWND window);
	void terminateGraphics();
	void renderGraphics();
	bool resizeGraphics(FLOAT width, FLOAT height);

	template<class T> void safeRelease(T** dxobject)
	{
		if (*dxobject != nullptr)
		{
			(*dxobject)->Release();
			*dxobject = nullptr;
		}
	}

	ID3D11Device*&				getDevice();
	ID3D11DeviceContext*&		getContext();
	ID3D11RenderTargetView*&	getTarget();
	IDXGISwapChain*&			getSwapChain();
}
