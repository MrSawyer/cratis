#pragma once

#include "window.h"
#include "graphics.h"

namespace Renderer
{
	bool	initialize(HINSTANCE instance);
	void	terminate();
	int		run();
}
