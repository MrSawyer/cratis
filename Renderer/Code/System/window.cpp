#include "window.h"

namespace Renderer::system
{
	HINSTANCE	Instance = 0;
	HWND		Window = 0;

	LRESULT CALLBACK windowProcedure(HWND window, UINT message, WPARAM wparam, LPARAM lparam);

	bool initializeWindow(HINSTANCE instance)
	{
		Instance = instance;

		WNDCLASSEX wcex = { 0 };
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = Instance;
		wcex.hCursor = LoadCursor(0, IDC_ARROW);
		wcex.hIconSm = LoadIcon(0, IDI_APPLICATION);
		wcex.hIcon = LoadIcon(0, IDI_APPLICATION);
		wcex.hbrBackground = reinterpret_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
		wcex.lpfnWndProc = windowProcedure;
		wcex.lpszClassName = "WINDOW";
		wcex.lpszMenuName = nullptr;
		wcex.style = CS_HREDRAW | CS_VREDRAW;

		if (!RegisterClassEx(&wcex))
		{
			MessageBox(0, "Registering window class failed.", "Error", MB_ICONERROR | MB_OK);
			terminateWindow();
			return false;
		}

		HWND desktop = GetDesktopWindow();

		RECT screenclient;
		GetClientRect(desktop, &screenclient);

		const FLOAT dpi = (FLOAT)GetDpiForWindow(desktop) / USER_DEFAULT_SCREEN_DPI;

		const int screen_width = static_cast<int>(screenclient.right - screenclient.left);
		const int screen_height = static_cast<int>(screenclient.bottom - screenclient.top);
		
		const int window_width = static_cast<int>(dpi * 1200);
		const int window_height = static_cast<int>(dpi * 600);

		Window = CreateWindowEx(0, "WINDOW", "Cratis", WS_OVERLAPPEDWINDOW, (screen_width - window_width) / 2, (screen_height - window_height) / 2, window_width, window_height, 0, 0, Instance, nullptr);
		if (Window == 0)
		{
			MessageBox(0, "Creating window failed.", "Error", MB_ICONERROR | MB_OK);
			terminateWindow();
			return false;
		}

		return true;
	}

	void terminateWindow()
	{
		if (Window != 0)
		{
			DestroyWindow(Window);
			Window = 0;
		}
	}

	void activateWindow()
	{
		ShowWindow(Window, SW_SHOW);
		UpdateWindow(Window);
	}

	LRESULT CALLBACK windowProcedure(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
	{
		static BOOL SIZE_RESTORED_NONMODALLY = TRUE;

		switch (message)
		{
		case WM_ENTERSIZEMOVE:
		{
			SIZE_RESTORED_NONMODALLY = FALSE;
			return 0;
		}

		case WM_SIZE:
		{
			if (wparam == SIZE_MAXIMIZED || SIZE_RESTORED_NONMODALLY)
			{
				const FLOAT width = static_cast<FLOAT>(LOWORD(lparam));
				const FLOAT height = static_cast<FLOAT>(HIWORD(lparam));

				if (!system::resizeGraphics(width, height))
				{
					MessageBox(0, "Window resize failed.", "Error", MB_ICONERROR | MB_OK);
					PostMessage(window, WM_CLOSE, 0, 0);
				}
			}

			return 0;
		}

		case WM_EXITSIZEMOVE:
		{
			RECT windowclient;
			GetClientRect(window, &windowclient);

			const FLOAT width = static_cast<FLOAT>(windowclient.right - windowclient.left);
			const FLOAT height = static_cast<FLOAT>(windowclient.bottom - windowclient.top);

			if (!system::resizeGraphics(width, height))
			{
				MessageBox(0, "Window resize failed.", "Error", MB_ICONERROR | MB_OK);
				PostMessage(window, WM_CLOSE, 0, 0);
			}

			SIZE_RESTORED_NONMODALLY = TRUE;

			return 0;
		}

		case WM_DPICHANGED:
		{
			RECT* windowclient = reinterpret_cast<RECT*>(lparam);

			const int x = static_cast<int>(windowclient->left);
			const int y = static_cast<int>(windowclient->top);
			const int w = static_cast<int>(windowclient->right - windowclient->left);
			const int h = static_cast<int>(windowclient->bottom - windowclient->top);

			SetWindowPos(window, 0, x, y, w, h, SWP_NOZORDER | SWP_NOACTIVATE);

			return 0;
		}

		case WM_DESTROY:
		{
			Window = 0;
			PostQuitMessage(0);
			break;
		}
		}

		return DefWindowProc(window, message, wparam, lparam);
	}

	const HWND& getWindowHandle()
	{
		return Window;
	}
}
