#pragma once

#include <Windows.h>

namespace Renderer::presets::storage
{
	bool initializeRectangle();
	void terminateRectangle();
}

namespace Renderer::presets
{
	void bindRectangle(UINT slot = 0);
	void drawRectangle(UINT slot = 0);
}
