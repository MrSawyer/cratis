#include "circle-preset.h"

#include "../System/window.h"
#include "../Components/vbo.h"

#include <vector>
#include <math.h>

namespace Renderer::presets::storage
{
	components::VBO* circle_vbo = nullptr;

	bool initializeCircle()
	{
		HWND window_handle = system::getWindowHandle();

		FLOAT radius = 0.5F;
		FLOAT step = 1.0F;

		std::vector<FLOAT> vertices;
		for (FLOAT i = 0; i < 360.0F; i += step)
		{
			FLOAT a = (FLOAT)(i * 3.14159265358979323846 / 180.0);
			FLOAT x = radius * cosf(a);
			FLOAT y = radius * sinf(a);
			FLOAT u = x + 0.5F;
			FLOAT v = y + 0.5F;

			vertices.push_back(x); vertices.push_back(y); vertices.push_back(u); vertices.push_back(v);
			vertices.push_back(0.0F); vertices.push_back(0.0F); vertices.push_back(0.5F); vertices.push_back(0.5F);

			i += step;

			a = (FLOAT)(i * 3.14159265358979323846 / 180.0);
			x = radius * cosf(a);
			y = radius * sinf(a);
			u = x + 0.5F;
			v = y + 0.5F;

			vertices.push_back(x); vertices.push_back(y); vertices.push_back(u); vertices.push_back(v);

			i -= step;
		}

		circle_vbo = components::createComponent<components::VBO>();
		circle_vbo->setBufferData(vertices, 4);
		circle_vbo->setBufferTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		if (!circle_vbo->makeBuffer())
		{
			MessageBox(window_handle, "Circle preset initialization failed.", "Error", MB_ICONERROR | MB_OK);
			terminateCircle();
			return false;
		}

		return true;
	}

	void terminateCircle()
	{
		components::destroyComponent(circle_vbo);
	}
}

namespace Renderer::presets
{
	void bindCircle(UINT slot)
	{
		storage::circle_vbo->bindBuffer(slot);
	}

	void drawCircle(UINT slot)
	{
		storage::circle_vbo->drawBuffer(slot);
	}
}
