#include "triangle-preset.h"

#include "../System/window.h"
#include "../Components/vbo.h"

#include <math.h>

namespace Renderer::presets::storage
{
	components::VBO* triangle_vbo = nullptr;

	bool initializeTriangle()
	{
		HWND window_handle = system::getWindowHandle();

		FLOAT a1_2 = 0.5F;
		FLOAT h1_2 = 0.5F * a1_2 * sqrtf(3.0F);

		FLOAT vertices[] =
		{
			-a1_2, -h1_2, 0.0F, 0.5F - h1_2,
			0.0F, h1_2, 0.5F, h1_2 + 0.5F,
			a1_2, -h1_2, 1.0F, 0.5F - h1_2
		};

		triangle_vbo = components::createComponent<components::VBO>();
		triangle_vbo->setBufferData(vertices, 12, 4);
		triangle_vbo->setBufferTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		if (!triangle_vbo->makeBuffer())
		{
			MessageBox(window_handle, "Triangle preset initialization failed.", "Error", MB_ICONERROR | MB_OK);
			terminateTriangle();
			return false;
		}

		return true;
	}

	void terminateTriangle()
	{
		components::destroyComponent(triangle_vbo);
	}
}

namespace Renderer::presets
{
	void bindTriangle(UINT slot)
	{
		storage::triangle_vbo->bindBuffer(slot);
	}

	void drawTriangle(UINT slot)
	{
		storage::triangle_vbo->drawBuffer(slot);
	}
}
