#pragma once

#include <Windows.h>

namespace Renderer::presets::storage
{
	bool initializeTriangle();
	void terminateTriangle();
}

namespace Renderer::presets
{
	void bindTriangle(UINT slot = 0);
	void drawTriangle(UINT slot = 0);
}
