#include "rectangle-preset.h"

#include "../System/window.h"
#include "../Components/vbo.h"

namespace Renderer::presets::storage
{
	components::VBO* rectangle_vbo = nullptr;

	bool initializeRectangle()
	{
		HWND window_handle = system::getWindowHandle();

		FLOAT vertices[] =
		{
			-0.5F, -0.5F, 0.0F, 0.0F,
			-0.5F, 0.5F, 0.0F, 1.0F,
			0.5F, -0.5F, 1.0F, 0.0F,
			0.5F, -0.5F, 1.0F, 0.0F,
			-0.5F, 0.5F, 0.0F, 1.0F,
			0.5F, 0.5F, 1.0F, 1.0F
		};

		rectangle_vbo = components::createComponent<components::VBO>();
		rectangle_vbo->setBufferData(vertices, 24, 4);
		rectangle_vbo->setBufferTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		if (!rectangle_vbo->makeBuffer())
		{
			MessageBox(window_handle, "Rectangle preset initialization failed.", "Error", MB_ICONERROR | MB_OK);
			terminateRectangle();
			return false;
		}

		return true;
	}

	void terminateRectangle()
	{
		components::destroyComponent(rectangle_vbo);
	}
}

namespace Renderer::presets
{
	void bindRectangle(UINT slot)
	{
		storage::rectangle_vbo->bindBuffer(slot);
	}

	void drawRectangle(UINT slot)
	{
		storage::rectangle_vbo->drawBuffer(slot);
	}
}
