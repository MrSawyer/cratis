#pragma once

#include <Windows.h>

namespace Renderer::presets::storage
{
	bool initializeCircle();
	void terminateCircle();
}

namespace Renderer::presets
{
	void bindCircle(UINT slot = 0);
	void drawCircle(UINT slot = 0);
}
