#include "testcode.h"

#include "System/window.h"
#include "System/graphics.h"

#include "Components/ubo.h"
#include "Components/shader.h"
#include "Components/texture.h"
#include "Components/sampler.h"

#include "Loaders/shader-loader.h"
#include "Loaders/texture-loader.h"

#include "Presets/rectangle-preset.h"
#include "Presets/triangle-preset.h"
#include "Presets/circle-preset.h"

#include <vector>
#include <math.h>
#include <time.h>

namespace Testcode
{
	void calculateWindowSurface(FLOAT& surface_width, FLOAT& surface_height);

	using namespace Renderer;
	using namespace Renderer::system;
	using namespace Renderer::components;
	using namespace Renderer::loaders;

	enum class DRAW_PRESET
	{
		RECTANGLE = 0,
		TRIANGLE = 1,
		CIRCLE = 2
	};
	DRAW_PRESET draw_preset = DRAW_PRESET::RECTANGLE;

	struct VertexShaderConstants : UniformData
	{
		FLOAT mvp[16] = { 0.0F };
		FLOAT scale[2] = { 500.0F, 500.0F };
		FLOAT unused[2];
	};
	VertexShaderConstants vs_constants;

	struct PixelShaderConstants : UniformData
	{
		FLOAT color[4] = { 1.0F, 1.0F, 1.0F, 1.0F };
	};
	PixelShaderConstants ps_constants;

	UBO*	vs_uniforms = nullptr;
	UBO*	ps_uniforms = nullptr;

	bool onInit()
	{
		Shader* shader = loaders::loadShaderFromFile("test-shader.hlsl");
		shader->appendInputLayoutElement("POS", DXGI_FORMAT_R32G32_FLOAT);
		shader->appendInputLayoutElement("UVS", DXGI_FORMAT_R32G32_FLOAT);
		shader->makeShader();
		shader->bindShader();

		Texture* texture = loaders::loadTextureFromFile("test-background.jpg");
		texture->makeTexture();
		texture->bindTextureToPixelShader();

		Sampler* sampler = createComponent<Sampler>();
		sampler->setTextureFilter(D3D11_FILTER_MIN_MAG_MIP_LINEAR);
		sampler->makeSampler();
		sampler->bindSamplerToPixelShader();

		vs_uniforms = createComponent<UBO>();
		vs_uniforms->setBufferData(vs_constants, sizeof(vs_constants));
		vs_uniforms->makeBuffer();
		vs_uniforms->bindBufferToVertexShader();

		ps_uniforms = createComponent<UBO>();
		ps_uniforms->setBufferData(ps_constants, sizeof(ps_constants));
		ps_uniforms->makeBuffer();
		ps_uniforms->bindBufferToPixelShader();

		return true;
	}

	void onUpdate()
	{
		FLOAT surface_width, surface_height;
		calculateWindowSurface(surface_width, surface_height);

		const FLOAT clipping_planes[] =
		{
			0.5F * surface_height, -0.5F * surface_width, -0.5F * surface_height, 0.5F * surface_width
		};

		vs_constants.mvp[0] = 2.0F / (clipping_planes[3] - clipping_planes[1]);
		vs_constants.mvp[5] = 2.0F / (clipping_planes[0] - clipping_planes[2]);
		vs_constants.mvp[10] = -1.0F;
		vs_constants.mvp[12] = -(clipping_planes[3] + clipping_planes[1]) / (clipping_planes[3] - clipping_planes[1]);
		vs_constants.mvp[13] = -(clipping_planes[0] + clipping_planes[2]) / (clipping_planes[0] - clipping_planes[2]);
		vs_constants.mvp[14] = 0.0F;
		vs_constants.mvp[15] = 1.0F;

		//vs_constants.scale[0] = surface_width;
		//vs_constants.scale[1] = surface_width * (1000.0F / 1468.0F);

		static FLOAT angle = 0.0F;
		angle += 1.0F;
		if (angle > 360.0F)
			angle -= 360.0F;

		const FLOAT radians = (FLOAT)(angle * 3.14159265358979323846 / 180.0);

		ps_constants.color[0] = 0.5F * sinf(radians) + 0.5F;
		ps_constants.color[1] = 0.5F * cosf(radians) + 0.5F;
		ps_constants.color[2] = 0.5F * cosf(radians) + 0.5F;
		ps_constants.color[3] = 1.0F;

		vs_uniforms->updateBufferData(vs_constants, sizeof(vs_constants));
		ps_uniforms->updateBufferData(ps_constants, sizeof(ps_constants));

		if (GetAsyncKeyState('1'))
		{
			draw_preset = DRAW_PRESET::RECTANGLE;
		}
		else if (GetAsyncKeyState('2'))
		{
			draw_preset = DRAW_PRESET::TRIANGLE;
		}
		else if (GetAsyncKeyState('3'))
		{
			draw_preset = DRAW_PRESET::CIRCLE;
		}
	}

	void onDraw()
	{
		switch (draw_preset)
		{
		case DRAW_PRESET::RECTANGLE:
			presets::drawRectangle();
			break;

		case DRAW_PRESET::TRIANGLE:
			presets::drawTriangle();
			break;

		case DRAW_PRESET::CIRCLE:
			presets::drawCircle();
			break;
		}
	}

	void calculateWindowSurface(FLOAT& surface_width, FLOAT& surface_height)
	{
		RECT window_client;
		GetClientRect(getWindowHandle(), &window_client);
		surface_width = (FLOAT)(window_client.right - window_client.left);
		surface_height = (FLOAT)(window_client.bottom - window_client.top);
	}
}
