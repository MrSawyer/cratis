#include "System/core.h"

#ifdef _DEBUG
int main(int argc, char* argv[])
{
	if (!Renderer::initialize(reinterpret_cast<HINSTANCE>(GetModuleHandle(nullptr))))
	{
		Renderer::terminate();
		::system("pause");
		return 0;
	}

	return Renderer::run();
}
#else
int WINAPI WinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE previnstance, _In_ LPSTR cmdline, _In_ int cmdshow)
{
	if (!Renderer::initialize(instance))
	{
		Renderer::terminate();
		return 0;
	}

	return Renderer::run();
}
#endif
