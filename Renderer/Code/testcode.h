#pragma once

namespace Testcode
{
	bool onInit();
	void onUpdate();
	void onDraw();
}
