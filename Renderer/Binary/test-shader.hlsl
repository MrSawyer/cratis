struct VSOutput
{
	float4 position : SV_POSITION;
	float2 uv : UVS;
};

cbuffer VSConstants : register(b0)
{
	float4x4	MVP;
	float2		scale;
}

VSOutput VSMain(float2 positions : POS, float2 uvs : UVS)
{
	VSOutput output;

	output.position = mul(float4(positions.x * scale.x, positions.y * scale.y, 0.0, 1.0), MVP);
	output.uv = uvs;

	return output;
}

cbuffer PSConstants : register(b0)
{
	float4 color;
}

Texture2D PSTexture : register(t0);

SamplerState PSSampler : register(s0);

float4 PSMain(VSOutput input) : SV_TARGET
{
	float4 image = PSTexture.Sample(PSSampler, input.uv);
	return image * color;
}
